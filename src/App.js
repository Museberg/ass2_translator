import './App.css';
import Login from './views/Login'
import Translator from './views/Translator'
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import Navbar from "./components/Navbar/Navbar";
import Profile from "./views/Profile"

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={ <Login /> } />
          <Route path="/translator" element={ <Translator /> } />
          <Route path="/profile" element={ <Profile /> } />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
