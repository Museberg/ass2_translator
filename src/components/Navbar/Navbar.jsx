import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"
import './Navbar.css';

const Navbar = () => {

    const { user } = useUser()

    return (
        <nav>
            <br></br><br></br>
            { user !== null &&
            <ul id="Nav_menu">
                <li>
                    <NavLink to="/translator" className="Nav_Link">Translator</NavLink>
                </li>
                <li>
                    <NavLink to="/profile" className="Nav_Link">Profile</NavLink>
                </li>
            </ul>
}
        <br></br><br></br><br></br>
        </nav>
    )

    
}

export default Navbar