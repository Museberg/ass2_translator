import { Form, Button } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { loginUser } from "../../api/user";
import { useState, useEffect } from "react";
import { storageSave } from "../../utils/storage";
import { useUser } from "../../context/UserContext";
import { useNavigate } from "react-router-dom";
import {STORAGE_KEY_USER} from "../../const/storageKeys";

const usernameConfig = {
  required: true,
  minLength: 3,
};

const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const { user, setUser } = useUser(null);
  const navigate = useNavigate();

  // Local state
  const [loading, setLoading] = useState(false);
  const [apirError, setApiError] = useState(null);

  // Side effects
  useEffect(() => {
    if (user !== null) {
      navigate("translator");
    }
  }, [user, navigate]);

  // Event handlers
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };

  // Render functions
  const errorMessage = (() => {
    switch (errors.username?.type) {
      case null:
        return null;
      case "minLength":
        return <span> Username is too short </span>;
      case "required":
        return <span> Username is required </span>;
      default:
        return null;
    }
  })();

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Form.Group className="mb-3">
        <Form.Label>Welcome!</Form.Label>
        <Form.Control
          type="text"
          placeholder="Enter username"
          {...register("username", usernameConfig)}
        />
        <Form.Text className="text-muted">
          We'll never share your translations with anyone else (except for JC).
        </Form.Text>
      </Form.Group>
      <Button variant="primary" type="submit" disabled={loading}>
        Continue
      </Button>
      {errorMessage}
      {loading && <p>Logging in</p>}
      {apirError && <p>{apirError}</p>}
    </Form>
  );
};

export default LoginForm;
