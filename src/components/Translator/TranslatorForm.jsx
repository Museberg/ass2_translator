import {
  Container,
  Col,
  Row,
  Card,
  FloatingLabel,
  Form,
  Button,
  Image
} from "react-bootstrap";

import { useState } from 'react'
import {storageSave} from "../../utils/storage";
import {translationAdd} from "../../api/translation";
import {useUser} from "../../context/UserContext";
import {STORAGE_KEY_USER} from "../../const/storageKeys";

const TranslatorForm = () => {
  const [userInput, setUserInput] = useState("");
  const [translatedInput, setTranslatedInput] = useState([])
  const { user, setUser } = useUser();

  const translateInput = async () => {
    // Removing all white space and converting to array
    const formattedTranslation = Array.from(userInput.replace(/\s/g,''));
    setTranslatedInput(formattedTranslation);

    // Saving to API
    const [ error, updatedUser] = await translationAdd(user, userInput);
    if (error !== null){
      return;
    }
    // Keep UI state and service state in sync
    storageSave(STORAGE_KEY_USER, updatedUser);
    console.log("Updated user: ", updatedUser)
    setUser(updatedUser);
  };

  return (
    <Container>
      <Row>
        <Col>
          <Card>
            <Card.Body>
              <FloatingLabel label="Input">
                <Form.Control
                  as="textarea"
                  placeholder="Leave a comment here"
                  style={{ height: "100px" }}
                  value={userInput}
                  onChange={(e) => setUserInput(e.target.value)}
                />
              </FloatingLabel>
            </Card.Body>
            <Card.Footer>
              <Button onClick={translateInput}>Translate</Button>
            </Card.Footer>
          </Card>
        </Col>

        <Col>
          <Card>
            <Card.Body>
              {
                translatedInput.map((letter, index) => (
                    <Image id={index} height="60px" src={`/images/individial_signs/${letter}.png`}/>
                ))
              }
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default TranslatorForm;
