import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";
  
const ProfileTranslationHistory = ({ translations }) => {

    
    const translationsList = translations.slice(-10).map(
        (translation, index) => <ProfileTranslationHistoryItem key={ translation + index } item={ translation } /> )

    return (
        <>
        <h2>Latest translations</h2>
        <section>
            <ol>
            { translationsList }
            </ol>
        </section>

        </>

    )
}

export default ProfileTranslationHistory