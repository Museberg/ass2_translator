import { Button } from 'react-bootstrap'
import {translationClear} from "../../api/translation";
import {useUser} from "../../context/UserContext";
import {storageSave, storageDelete} from "../../utils/storage";
import {STORAGE_KEY_USER} from "../../const/storageKeys";

const ProfileActions = () => {
    const { user, setUser } = useUser()

    const handleLogoutClick = () => {
        if (window.confirm('Are you sure?')) {
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    const handleClearHistoryClick = async () => {
        if (!window.confirm('Are you sure?\nThis cannot be undone!'))
            return

        const [ clearError ] = await translationClear(user.id)

        if (clearError !== null){
            console.log(clearError)
        }

        const updatedUser = {
            ...user,
            translations: []
        };

        storageSave(STORAGE_KEY_USER, updatedUser);
        setUser(updatedUser);
    }

    return (
        <ul>
            <br></br><br></br>
            <li className="my-4" onClick={ handleClearHistoryClick }><Button>Clear translation history</Button></li>
            <li><Button onClick={ handleLogoutClick }>Log out</Button></li><br></br><br></br><br></br>
        </ul>
    )
}

export default ProfileActions