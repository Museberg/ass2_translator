import ProfileActions from "../components/Profile/ProfileActions"
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory"
import { useUser } from "../context/UserContext"
import { storageSave } from "../utils/storage";
import {STORAGE_KEY_USER} from "../const/storageKeys";
import withAuth from '../hoc/withAuth'

const Profile = () => {

    const { user } = useUser()

    const logout = () => {
        storageSave(STORAGE_KEY_USER, null)
    }

    return (
        <>
            <h1>Profile</h1>
            <h2>Username: { user.username }</h2><br></br>
            <ProfileTranslationHistory translations={ user.translations } />
            <ProfileActions logout={ logout } />
            
        </>
    )
}

export default withAuth(Profile)