import LoginForm from "../components/Login/LoginForm";
import { Card } from "react-bootstrap";

const center = {
  margin: "auto",
  width: "50%",
  padding: "10px",
};

const Login = () => {
  return (
    <div style={center}>
      <Card>
        <Card.Body>
          <h1>ASL Translator</h1>
          <LoginForm />
        </Card.Body>
      </Card>
    </div>
  );
};

export default Login;
