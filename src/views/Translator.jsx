import TranslatorForm from "../components/Translator/TranslatorForm";
import withAuth from "../hoc/withAuth";
import { useUser } from "../context/UserContext";

const Translator = () => {

  const { user } = useUser()

  return (
    <>
    <h1>ASL Translator</h1>
    <h2>Username: { user.username }</h2> <br></br>
      <TranslatorForm />
    </>
  );
};

export default withAuth(Translator)
