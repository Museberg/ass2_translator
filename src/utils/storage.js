export const storageSave = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const storageRead = key => {
    const data = localStorage.getItem(key);
    return data ? JSON.parse(data) : null;
}

export const storageDelete = key => {
    localStorage.removeItem(key);
}