# Text to sign language translator

We have made this simple web app that lets you translate any text to sign language.
It has a global state that is managed via your browser's cookies.


### Installation
To install and run the program, first type in `npm install` to install all of the npm dependencies.
Once the installtion has finished, the program can be started by typing `npm start`.


### First usage
The first time you visit the page, you will be met with a log on screen - here you can enter any name.
If a user with your name already exists, you will simply be logged in and presented with the translation screen.

If a user with the given username does not already exists, a new user with that username will be created.

### Profile page
On your profile page you can see your 10 last translations.
You are also presented with a clear button, that will clear your entire translation history.

It is also from this page that you are able to log out.


### Contributers
This translation app was made in collaboration between Mikael Frykman and Rasmus Falk-Jensen
